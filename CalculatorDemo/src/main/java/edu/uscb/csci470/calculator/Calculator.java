package edu.uscb.csci470.calculator;

/**
 * Calculator class that implements basic 
 * arithmetic operations as named methods
 * 
 * @author your_username@email.uscb.edu
 * @version 0.1
 */
public class Calculator {
	
	public int add(int a, int b ) {
		return a+b;
	} // end method add
	  
/**
 * 
 * @int a
 * @int b
 * @return the subtraction
 */
		
	public int subtract(int a, int b) {
		return a-b;
	}
	/**
	 * long is used because when we multiply 2 large integers the result will also be huge 
	 * @param a
	 * @param b
	 * @return long 
	 */
	public long multiply(int a, int b) {
		return a*b;
	}

	/**
	 * Note that this method throws an expectation when b is zero
	 * @param a
	 * @param b
	 * @return
	 */
	public int intDivide(int a, int b) {
		int result;
		if (b==0) {
			throw new IllegalArgumentException
				("intDivide method: cannot divide by zero");
			}else {
				result = a/b;
			} // end if/else
			return result;
		
		
	 }// end method IntDivide
	
	/**
	 * 
	 * Return the floating point division result of 
	 * dividing a by b. Note that this method throws
	 * an exception when b == 0
	 * @param a the dividend or numerator
	 * @param b the divisor or deniminator
	 * return 
	 */
	public double divide(int a, int b) {
		double result;
		if (b==0) {
			throw new IllegalArgumentException
				("intDivide method: cannot divide by zero");
			}else {
				result = (double)a/(double)b;
			} // end if/else
			return result;
		
	} // end method divide
	
	
} // end class Calculator
