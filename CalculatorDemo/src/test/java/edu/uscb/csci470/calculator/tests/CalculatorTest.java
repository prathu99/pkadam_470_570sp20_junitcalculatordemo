package edu.uscb.csci470.calculator.tests;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.*;

import edu.uscb.csci470.calculator.Calculator;

// NOTE: make sure your pom.xml file has the necessary dependencies
//   so that Eclipse knows what to import!

public class CalculatorTest {
   
    private Calculator calculatorTestInstance; // has class-wide scope, private to class
    private static Logger logger = LoggerFactory.getLogger(CalculatorTest.class);
    
    @BeforeClass
    public static void setUpBeforeAllTests() {
    	logger.info("Starting tests for this class...");
    }
    
    @Before
    public void setUpBeforeEachTest() {
    	calculatorTestInstance = new Calculator();
    	logger.info("new test instance: " + calculatorTestInstance );
    } // end method SetUpBeforeEachTest
    
    @After
    public void teardownAfterEachTest() {
    	logger.info("...a test was just run...");
    } // end method teardownAfterEachTest()
    
    @AfterClass
    public static void teardownAfterAllTests() {
    	logger.info("All tests completed, closing DB connections etc.");
    } // end method teardownAfterAllTests
    
    
    @Test
    public void testAdd() {
    	int value1 = 25;
    	int value2 = 10;
    	int expectedResult = 35;
    	int testResult = calculatorTestInstance.add(value1, value2);
    	Assert.assertEquals(expectedResult, testResult);
    } //end test method testAdd
    
   @Test
    public void testSubtract() {
	int value1 = 10;
    int value2 = 5;
    int expectedResult = 5;
    int testResult = calculatorTestInstance.subtract(value1, value2);
    Assert.assertEquals(expectedResult, testResult);
    }
  
   @Test
   public void testMultiply() {
   int value1 = 10;
   int value2 = 5;
   int expectedResult = 50;
   long testResult = calculatorTestInstance.multiply(value1, value2);
   Assert.assertEquals(expectedResult, testResult);
   }
   
   @Test
   public void testIntDivide() {
   int value1 = 10;
   int value2 = 5;
   int expectedResult = 2;
   int testResult = calculatorTestInstance.intDivide(value1, value2);
   Assert.assertEquals(expectedResult, testResult);
   }

	@Test( expected = IllegalArgumentException.class)
	public void testIntDivideByZero() {
		int value1 = 25;
		int value2 = 0;
		try {
			calculatorTestInstance.intDivide(value1, value2);
		} finally {
			System.out.println("testDivideByZero complete");
		}// end try statement
	}// end test method testIntDivide by Zero
	
	@Test
	public void testDivide() {
		int value1 = 25;
		int value2 = 10;
		double tolerance = 0.00005;
		double expectedResult = 2.5;
		double testRestult = calculatorTestInstance.divide(value1, value2);
		Assert.assertEquals(expectedResult, testRestult, tolerance);
		
	} // end test method testDivide
} // end class CalculatorTest
